includeTargets << new File(backbonePluginDir, 'scripts/_BackboneCommon.groovy')

target(createBackboneModel: "Create backbone model") {

    def modelName = argsMap.params[0]

    renderBackboneTemplate('models', modelName, [modelName: modelName])
}

setDefaultTarget(createBackboneModel)
